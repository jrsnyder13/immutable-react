# immutable-react #

Experiments with using immutable data (via [Mori](https://github.com/swannodette/mori)) for an idempotent interface to React.