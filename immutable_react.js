"use strict";

var m = mori;

var lenses = {
	create: function(tree, path) {
		return m.hash_map(
			"path", path,
			"value", m.get_in(tree, path),
			"root", tree
		);
	},
	down: function(lens, key) {
		return m.hash_map(
			"path", m.conj(m.get(lens, "path"), key),
			"value", m.get(m.get(lens, "value"), key),
			"root", m.get(lens, "root")
		);
	},
	downPath: function(lens, path) {
		return m.hash_map(
			"path", m.into(m.get(lens, "path"), path),
			"value", m.get_in(m.get(lens, "value"), path),
			"root", m.get(lens, "root")
		);
	},
	set: function(lens, value) {
		return m.assoc_in(m.get(lens, "root"), m.get(lens, "path"), value);
	}
};

function printData(data) {
	console.log(m.clj_to_js(data));
}

var ImmutablePropsMixin = {
	// Only individual props can be Mori objects.
	// React shallow clones the props object before passing it to components, stripping the Mori prototype, causing errors.
	shouldComponentUpdate: function(nextProps, nextState) {
        var props = this.props;
		//Compare each prop in nextProps and this.props
		function propEqual(key) {
            var a = props[key];
            var b = nextProps[key];

            if (m.is_collection(a)) {
                return m.equals(a, b);
            } else {
                return a === b;
            }
        }
		return !m.every(propEqual, Object.keys(this.props)) || !m.equals(this.state, nextState);
	}
};