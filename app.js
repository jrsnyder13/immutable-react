"use strict";

var m = mori;


var AppView = React.createClass({displayName: "App",
    mixins: [ImmutablePropsMixin],
    render: function() {
        // This is only necessary in topmost component. (Props object itself cannot be a mori object)
        var props = this.props.mProps;
        var checklistName = lenses.create(props, ['groups', 0, 'checklists', 0, 'name']);
        return React.DOM.input({
            type: "text",
            onChange: this.textChange.bind(this, checklistName),
            value: m.get(checklistName, 'value')
        });
    },
    textChange: function(lens, event) {
        setUI(lenses.set(lens, event.target.value));
    }
});


function setUI(props) {
    React.renderComponent(
	    AppView({mProps: props}),
	    document.body
	);
}

var initialProps = m.js_to_clj({
    name: "File",
    groups: [
        {
            name: "Group 1",
            checklists: [
                {
                    name: "Checklist 1"
                },
                {
                    name: "Checklist 2"
                }
            ]
        },
        {
            name: "Group 2"
        }
    ]
});

setUI(initialProps);